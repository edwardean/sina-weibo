from core  import bases
import os
import logging
from base import NotificationBase

logger=logging.getLogger("Notification")

class Notification(NotificationBase):
    """Notification"""
    __model=None

    def __init__(self,app_name):
        super(Notification, self).__init__(app_name)
        __impl__=self.get_support_notification()
        self.__notification=__impl__(app_name)

    def get_support_notification(self):
        try:
            if not Notification.__model:
                import notify_gi
                Notification.__model=notify_gi
        except Exception,e:
            logger.debug("Load notification model notify_gi failed:{0}".format(e))

        try:
            if not Notification.__model:
                import notify_pynotify
                Notification.__model=notify_pynotify
        except Exception,e:
            logger.debug("Load notification model notify_pynotify failed:{0}".format(e))

        if not Notification.__model:
            import notify_dummy
            Notification.__model=notify_dummy
        logger.info("Notification model is {0}".format(Notification.__model.__name__))
        return Notification.__model.Notification

    def update(self,*args,**kwargs):
        if not self.__notification:
            logger.debug("Notification not support on this platform")
        else:
            self.__notification.update(*args,**kwargs)

    def show(self):
        if self.__notification:
            self.__notification.show()

    def set_image(self,image):
        if self.__notification:
            self.__notification.set_image(image)

