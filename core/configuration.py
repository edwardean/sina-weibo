#!/usr/bin/env python
#encoding=utf-8

import os
from bases import JSLikeObject

def parent_dir(path):
    return os.path.dirname(os.path.abspath(os.path.join(path,os.path.pardir)))

class Configuration(JSLikeObject):
    def __init__(self):
        super(Configuration,self).__init__()
        self.resources_dir=os.path.join(parent_dir(__file__),"resources")

config=Configuration()