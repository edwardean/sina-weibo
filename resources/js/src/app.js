!function (global, $) {
	var App = {
		Weibo:{},
		Views:{},
		Controllers:{},
		Models:{},
		Collections:{},
		AccountTypes:[],
		Cache:new global.CacheProvider(),
		Widgets:{},
		Pages:{},
		Utils:{},
		Elements:{
			$body:$('body'),
			$mainLoading:$('#main_loading')
		},
		Actions:{
			showLoading:function () {
				App.Elements.$mainLoading.show();
			},
			hideLoading:function () {
				App.Elements.$mainLoading.hide();
			}
		},
		ProcessAjaxs:{},
		initialize:function () {
			this.appCtrl = new App.Controllers.AppCtrl();
			Backbone.history.start();
		}
	};

	//Accounts
	App.Accounts = (function(){
		var accounts = App.Cache.get('accounts', true, true) || {},
			get = function(id){
				return id ? accounts[id] : accounts;
			},
			add = function(account){
				accounts[account._id] = account;
				App.Cache.set('accounts', accounts, true);
				App.Accounts.trigger("add", account);
				return account;
			};
			
		return _.extend({
			get: get,
			add: add
		}, Backbone.Events);
	})();

	// controllers
	App.Controllers.AppCtrl = Backbone.Router.extend({
		routes:{
			'':'home',
			'!/':'home'
		},
		home:function () {
			var home = new App.Views.Home();
			home.render();
		}
	});

	global.App = App;
}(window, window.jQuery);