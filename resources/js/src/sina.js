!
function(global, App) {
	App.Views.Sina = {};
	App.Widgets.Sina = {};

	App.Weibo.Sina = {
		type: 'Sina',
		label: 'Sina',
		appKey: '2220221549',
		appSecret: 'd75ef035dda834cf7a60680388ebbf54',
		url: {
			api: 'https://api.weibo.com/2',
			authorize: (function() {
				return 'https://api.weibo.com/oauth2/authorize?' + $.param({
					display: 'mobile',
					client_id: '2220221549',
					redirect_uri: 'https://api.weibo.com/oauth2/default.html'
				});
			})(),
			token: 'https://api.weibo.com/oauth2/access_token',
			code: 'https://api.weibo.com/oauth2/default.html',
			user: {
				show: 'https://api.weibo.com/2/users/show.json'
			},
			reply: {
				show: 'https://api.weibo.com/2/comments/show.json'
			},
			status: {
				homeTimeline: 'https://api.weibo.com/2/statuses/home_timeline.json',
				show : 'https://api.weibo.com/2/statuses/show.json'
			}
		},
		getUserById: function(uid, accessToken, callback){
			var user = new App.Models.Sina.User();
			user.fetch({
				data: {
					access_token: accessToken,
					uid: uid
				},
				success: callback
			});
		},
		getAccessToken: function(code, callback) {
			$.post(App.Weibo.Sina.url.token, {
				client_id: App.Weibo.Sina.appKey,
				client_secret: App.Weibo.Sina.appSecret,
				grant_type: 'authorization_code',
				code: code,
				redirect_uri: App.Weibo.Sina.url.code
			}, function(res) {
				callback && callback(res);
			}, 'json');
		}
	};

	App.AccountTypes.push(App.Weibo.Sina);
}(window, window.App);