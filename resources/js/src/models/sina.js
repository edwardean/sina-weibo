!function (global, App) {
	App.Models.Sina = {
		Tweet:Backbone.Model.extend({
			url: App.Weibo.Sina.url.status.show,
			defaults:{
				prettyCreatedTime:function () {
					return App.Utils.prettyDate(this.created_at);
				},
				createdTime:function () {
					return new Date(this.created_at).format('yyyy-mm-dd HH:MM:ss');
				}
			}
		}),
		Reply:Backbone.Model.extend({
			url: App.Weibo.Sina.url.reply.show,
			defaults:{
				prettyCreatedTime:function () {
					return App.Utils.prettyDate(this.created_at);
				},
				createdTime:function () {
					return new Date(this.created_at).format('yyyy-mm-dd HH:MM:ss');
				}
			}
		}),
		User: Backbone.Model.extend({
			url: App.Weibo.Sina.url.user.show
		})
	};
}(window, window.App);