!function (global, App) {
	App.Views.Home = App.Views.Base.extend({
		events:{
			'click [data-action="settings-menu"]' : 'showSettingsMenu'
		},
		template:'home.html',
		container:App.Elements.$body,
		afterRender:function () {
			var accounts = App.Accounts.get();
			if(accounts && _.size(accounts)){
				var columns = App.Cache.get('columns', true, true);
				if (columns && columns.length) {

				} else {
					var account = _.find(accounts, function(){return true;}),
						homeTimeline = new App.Views[account.type].HomeTimeline({
							account:account,
							container:this.$('#columns')
						});
					homeTimeline.render();
				}
			}else{
				this.showSettingsMenu();
				App.Accounts.once("add", _.bind(function(account){
					var homeTimeline = new App.Views[account.type].HomeTimeline({
							account:account,
							container:this.$('#columns')
						});
					homeTimeline.render();
				}, this));
			}
		},
		showSettingsMenu: function(){
			var settings = new App.Views.Settings();
			settings.render();
		}
	});
}(window, window.App);