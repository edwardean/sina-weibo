!function (global, App) {

	/**
	 * 列表基类
	 */
	App.Views.Column = App.Views.Base.extend({
		events: {

		},
		//模板路径
		template:'column.html',
		tagName:"section",
		className:'column',
		fetch:function(){
			this.renderMain({title:this.title});
		},
		afterRender:function () {
			this.$header = this.$('[data-selector="header"]');
			this.$options = this.$('[data-selector="options"]');
			this.$stream = this.$('[data-selector="stream"]');
			this.$scroll = this.$('.scroll-v');
			this.$detail = this.$('[data-selector="detail"]');
			this.$loading = this.$('[data-selector="loading"]');
		}
	});
}(window, window.App);