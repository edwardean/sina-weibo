#!/usr/bin/python
#coding=utf-8

#Author asy
#Date 2011-03-12
#License LGPL
#这个类用来解析命令行参数，参数的形式为:
#第一位是Action，该参数和要调用的处理函数绑定，
#后面依次是key=value这种形式的参数列表，该参数列表
#将会被当作处理函数的参数传给处理参数


try:
	callable(None)
except:
	def callable(func):
		return func and hasattr(func,"__call__")

class UnknownAction(Exception):
	pass

class ArgParser(object):
	'''
		参数解析类:参数的形式action key=value,...
	'''
	def __init__(self,default=None):
		'''
		@default:默认的处理函数
		'''
		super(ArgParser,self).__init__()
		self.__cmd_bind=None
		self.default=default

	def bind(self,cmd_bind):
		'''
		绑定action与处理函数
		@cmd_bind:action和处理函数对应关系字典
		'''
		self.__cmd_bind=dict() if cmd_bind==None else cmd_bind

	def feed(self,args):
		'''
		@args:参数
		'''
		action=None
		if len(args)>=2:
			action=args[1]
		if action and action in self.__cmd_bind:
			kwarg=self.__parse_args(args[2:])
			self.__cmd_bind[action](**kwarg)
		elif action:
			raise UnknownAction("Unknown Action:{0}".format(action))
		else:
			if callable(self.default):
				self.default()

	@property
	def actions(self):
		'''
		返回支持的actions
		'''
		return self.__cmd_bind.keys()

	def __parse_args(self,args):
		kwarg=dict()
		for i in args:
			k=i.split("=")
			if k[0]:
				kwarg[k[0]]="" if len(k)<2 else "=".join(k[1:])
		return kwarg